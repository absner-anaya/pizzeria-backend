/**
 * ClientController
 *
 * @description :: Server-side logic for managing clients
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    create: function (req, res){
        
        console.log(req.body);
        if (Object.keys(req.body).length === 0){
            res.json({
                code: 200,
                message: 'No hay data'
            })
        }else{

            Client.create(req.body
            ).exec(function (err, data) {
                if(err) return res.json({ err: err }, 500);
                else res.json({
                    code: 200,
                    message: 'success',
                    content: data
                });
            })

        }
        
    },
    find: function (req, res) {
        
        Client.find().exec(function (err, users) {
            if(err) return res.json({ err: err }, 500);
            else res.json(users);
        });
    },
    findOne: function (req, res) {
        if (req.params.id === undefined ){
            res.json({
                code: 200,
                message: 'no ah proporcionado un id'
            })
        }else{
            
            Client.findOne({ id: req.params.id }).exec(function (err, user) {
                if(err) return res.json({ err: err }, 500);
                else res.json(user);
            });

        }
    },
    destroy: function (req, res) {

        if (req.params.id === undefined) {
            res.json({
                code: 200,
                message: 'no ha proporcionado un id'
            })
        }else{
            Client.destroy({id: req.params.id}).exec(function (err, user) {
                if(err) return res.json({ err: err }, 500);
                else res.json({
                    code: 200,
                    message: 'user delete',
                    content: user
                });
            });
        }
    },
    update: function (req, res) {
        if (req.params.id === undefined) {
            res.json({
                code: 200,
                message: 'no ha proporcionado un id'
            })
        }else{
            Client.update(
                {id: req.params.id},
                req.body
            ).exec(function (err, data) {
                if(err){ 
                    return res.json({ err: err }, 500);
                }else { 
                    
                    return res.json({
                        code: 200,
                        message: 'Update data',
                        content: data[0]
                    });
                }
            });
        }
        
    }

        
	
};

